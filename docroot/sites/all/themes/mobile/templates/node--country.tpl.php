
<div class="countrybody">
<div class="countryleftheader">



<!-- Print PHP code for main text  in country header--> 


</div>

     <!--- insert code for TOP LEFT  section here --->   
<div id="country_box"><?php
	    print render($content['field_left_top']);
// print $node->field_left_top_links[0]['value'];

?>

<?php if (isset($content['field_left_top_links'])): ?>

	<?php 
		drupal_add_js('misc/form.js');
	     drupal_add_js('misc/collapse.js');
	?>

    <fieldset class="collapsible collapsed">
	  <legend><span class="fieldset-legend">More</span></legend>
	<div class="fieldset-wrapper"> <?php print render($content['field_left_top_links']); ?> </div>  





</fieldset>

<?php endif; ?>  
</div> <!--- END Country Box --->   

<!--- insert code for TOP MIDDLE section here --->    
<div id="country_box">
<?php print render($content['field_middle_top']); ?>

<?php if (isset($content['field_middle_top_links'])): ?>

	<?php drupal_add_js('misc/collapse.js'); ?>
	<fieldset class="collapsible collapsed">
	  <legend><span class="fieldset-legend">More</span></legend>
	<div class="fieldset-wrapper"> <?php print render($content['field_middle_top_links']); ?> </div>

</fieldset>

<?php endif; ?>
</div> <!--- END Country Box ---> 

<!--- insert code for TOP RIGHT section here --->  
<div id="country_box">
<?php print render($content['field_right_top']); ?>

<?php if (isset($content['field_right_top_links'])): ?>

	<?php drupal_add_js('misc/collapse.js'); ?>
	<fieldset class="collapsible collapsed">
	  <legend><span class="fieldset-legend">More</span></legend>
	<div class="fieldset-wrapper"> <?php print render($content['field_right_top_links']); ?> </div>

</fieldset>

<?php endif; ?>
</div> <!--- END Country Box ---> 

<!--- insert code for BOTTOM LEFT section here ---> 
<div id="country_box">
<?php print render($content['field_left_bottom']); ?>

<?php if (isset($content['field_left_bottom_links'])): ?>

	<?php drupal_add_js('misc/collapse.js'); ?>
	<fieldset class="collapsible collapsed">
	  <legend><span class="fieldset-legend">More</span></legend>
	<div class="fieldset-wrapper"> <?php print render($content['field_left_bottom_links']); ?> </div>

</fieldset>

<?php endif; ?>
</div> <!--- END Country Box ---> 

<!--- insert code for BOTTOM MIDDLE section here ---> 
<div id="country_box">
<?php print render($content['field_middle_bottom']); ?>

<?php if (isset($content['field_middle_bottom_links'])): ?>

	<?php drupal_add_js('misc/collapse.js'); ?>
	<fieldset class="collapsible collapsed">
	  <legend><span class="fieldset-legend">More</span></legend>
	<div class="fieldset-wrapper"> <?php print render($content['field_middle_bottom_links']); ?> </div>

</fieldset>

<?php endif; ?>
</div> <!--- END Country Box ---> 

<!--- insert code for BOTTOM RIGHT section here --->  
<div id="country_box">   
<?php print render($content['field_right_bottom']); ?>

<?php if (isset($content['field_right_bottom_links'])): ?>

	<?php drupal_add_js('misc/collapse.js'); ?>
	<fieldset class="collapsible collapsed">
	  <legend><span class="fieldset-legend">More</span></legend>
	<div class="fieldset-wrapper"> <?php print render($content['field_right_bottom_links']); ?> </div>

</fieldset>

<?php endif; ?>
</div> <!--- END Country Box ---> 

<div class="countrytopbox"> <!-- Print PHP code for box -->

<?php
   print render($content['field_box_content']);

?>

 </div>
</div>
