<?php
// $Id: node.tpl.php,v 1.5 2007/10/11 09:51:29 goba Exp $
?>
<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?>">

<?php // print $picture ?>

<?php if ($page == 0): ?>
  <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
<?php endif; ?>

  <?php if ($submitted): ?>
    <span class="submitted"><?php // print $submitted; ?></span>
  <?php endif; ?>

 <div class="clear-block">
 <div class="meta">

    <?php if ($links): ?>
	<div class="links"><?php print $links; ?></div>
	<?php endif; ?>
 



    </div>

<div class="content clear-block">


   
   
   <div class="program-logos">
   		<?php print render($content['field_logos']); ?>
	</div>
   
   
<div id="bodyinset4">
   
     <div class="apply-now">
    <?php 
	global $language;
			if ($language->language == 'ar')
				{
				$anbsrc = "<img src='/sites/default/files/images/hq/misc/apply_now_ar.png' />";
				$rnbsrc = "<img src='/sites/default/files/images/hq/misc/register_now_ar.png' />";
				}
					
			else 
				{
				 $anbsrc = "<img src='/sites/default/files/images/hq/misc/apply.gif' />";
				 $rnbsrc = "<img src='/sites/default/files/images/hq/misc/registernow.png' />";
				}
			
	?>
   		<?php 
		if ($node->field_recruiting['und'][0]['value'] == 1):?>
        	
		<?php 
			
		if (!empty($field_application[0]['filename'])): ?>
   			<a href="<?php $base_url?>/sites/default/files/<?php print $field_application[0]['filename']; ?>" onclick="_gaq.push(['_trackEvent', 'Apply Now', 'Click']);" target="_blank">
                         <?php if ($node->field_buttontype['und'][0]['value'] == 'apply')
			 {
				 print $anbsrc;
				 }
				 else if ($node->field_buttontype['und'][0]['value'] == 'register')
				 {
				print $rnbsrc;	 
					 }
					else print $anbsrc;
					
           ?>
           </a>
		   
            
			<?php endif; ?>
            
            <?php if (!empty($field_app_link[0]['value'])): ?>
   			<a href="http://<?php print $field_app_link[0]['value']; ?>" onclick="_gaq.push(['_trackEvent', 'Apply Now', 'Click']);" target="_blank">
            
              <?php if ($node->field_buttontype['und'][0]['value'] == 'apply')
			 {
				 print $anbsrc;
				 }
				 else if ($node->field_buttontype['und'][0]['value'] == 'register')
				 {
				print $rnbsrc;	 
					 }
					else print $anbsrc;
					
           ?>
           </a>
            
			<?php endif; ?>
            
            
           
            
            
            
            
            <?php endif; ?>
            
            <?php if ($node->field_recruiting['und'][0]['value'] == 0):?>
				<div class="not_recruiting"><?php print "We are not currently accepting applications. Please check this page for updates."; ?></div>
				<?php endif; ?>
      	

   		
   </div>
    <div class="more-info">
   
        
		<?php if (!empty($field_program[0]['value'])): ?>
   			<a href="/<?php print $field_program[0]['value']; ?>" ><img src="/sites/default/files/images/hq/misc/moreinfo.gif"></a>
            
            
            
			<?php endif; ?>
        
            
           
	
   </div>
   </div>
   
     <div class="intro-paragraph">
	 	<?php print render($content['field_intro_p']); ?>   
   
  	 </div>
 
   

   
   
   <?php print render($content['body']); ?>
   
</div>


    <?php if (isset($taxonomy)): ?>
	<div class="terms"> Tagged under: <?php print $terms ?></div>
         <?php endif;?>
    </div>


</div>