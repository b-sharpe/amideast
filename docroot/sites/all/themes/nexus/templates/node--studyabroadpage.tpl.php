<?php
// $Id: node.tpl.php,v 1.5 2007/10/11 09:51:29 goba Exp $
?>
<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?>">

<?php if ($page == 0): ?>
  <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
<?php endif; ?>


 <div class="clear-block">
 			<div class="meta">

    <?php if (isset($links)): ?>
	<div class="links"><?php print $links; ?></div>
	<?php endif; ?>
 

   			</div> <!-- /meta -->

<div class="content clear-block">

<!-- content editbale begins here -->


<?php 
$block = block_load('menu_block', '16');
print render(_block_get_renderable_array(_block_render_blocks(array($block))));
?>

  <?php print render($content['body']); ?>
  <br /><br />
  
<?php
  $wrapper = entity_metadata_wrapper('node', $node);
  $formtype = field_get_items('node', $node, 'field_test_field_collection');
   foreach($formtype as $itemid) { 
              $item = field_collection_field_get_entity($itemid);
			  $output = $item->field_expand_collapse['und'][0]['safe_value'];
			  $question = $item->field_expand_question['und'][0]['safe_value'];
			  $options = array('handle' => $question, 'content' => $output, 'collapsed' => TRUE);
			 echo '<div id="questions_answers">';
			print theme('ctools_collapsible',$options);
			 echo '</div>';

}
?>
  
  
   <!-- content editbale ends here -->
   
 
   
   
   
</div> <!--/.content clear block -->


    
    </div> <!-- /.clear-block -->


</div> <!-- / #node-[nid] -->
