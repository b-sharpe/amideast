<?php
// $Id: node.tpl.php,v 1.5 2007/10/11 09:51:29 goba Exp $
?>
<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?>">

<?php // print $picture ?>

<?php if ($page == 0): ?>
  <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
<?php endif; ?>

  <?php if ($submitted): ?>
    <span class="submitted"><?php // print $submitted; ?></span>
  <?php endif; ?>

 <div class="clear-block">
 			<div class="meta">

	<div class="links"><?php print render($content['links']);?></div>
       
   			</div> <!-- /meta -->

<div class="content clear-block">

<!-- content editbale begins here -->
	<div id="profile2rightwrap">
	<div id="profile2rightbox">	<?php print render ($content['field_student_photo']); ?>
		
	</div><?php print render($content['field_mp3_file']); ?>
    <div id="playlist">
		<ul class="playlist2">
			<li>
			<?php 
            $items = field_get_items('node', $node, 'field_mp3_file');
            foreach ($items as $item) {$value = $item['fid'];}	?>
            <a href="/download/file/fid/<?php print $value?>">Download Podcast</a>
            </li>
        </ul>
    </div>



<br />
   
</div>
  <?php print render ($content['body']); ?>
  

  
  
  
   <!-- content editbale ends here -->
   
   
   
   
   
   
   
   
</div> <!--/.content clear block -->


    
    </div> <!-- /.clear-block -->


</div> <!-- / #node-[nid] -->
