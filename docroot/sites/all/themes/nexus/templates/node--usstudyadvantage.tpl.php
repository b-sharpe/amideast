<?php
// $Id: node.tpl.php,v 1.5 2007/10/11 09:51:29 goba Exp $
?>
<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?>">

<?php if ($page == 0): ?>
  <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
<?php endif; ?>


 <div class="clear-block">
 			<div class="meta">

    <?php if (isset($links)): ?>
	<div class="links"><?php print $links; ?></div>
	<?php endif; ?>
 

   			</div> <!-- /meta -->

<div class="content clear-block">

<!-- content editbale begins here -->


<?php 
$block = block_load('menu_block', '20');
print render(_block_get_renderable_array(_block_render_blocks(array($block))));
?>

  <?php print render($content['body']); ?>
  <br /><br />
  
  
   <!-- content editbale ends here -->
   
 
   
   
   
</div> <!--/.content clear block -->


    
    </div> <!-- /.clear-block -->


</div> <!-- / #node-[nid] -->
