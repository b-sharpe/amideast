<?php
// $Id: node.tpl.php,v 1.5 2007/10/11 09:51:29 goba Exp $
?>
<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?>">

<?php // print $picture ?>

<?php if ($page == 0): ?>
  <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
<?php endif; ?>

  <?php if ($submitted): ?>
    <span class="submitted"><?php // print $submitted; ?></span>
  <?php endif; ?>

 <div class="clear-block">
 			<div class="meta">

	<div class="links"><?php print render($content['links']);?></div>
       
 

   			</div> <!-- /meta -->

<div class="content clear-block">

<!-- content editbale begins here -->

<div id="profile2rightwrap">
	<div id="profile2rightbox">
		<?php print render ($content['field_student_photo']); ?>
		<div id="profile2name"><?php print $title; ?></div>
		<div id="profile2program"><span class="label">From:</span><?php print $field_country[0]['value']; ?><br />
		<span class="label">Employed at:</span><?php print $field_employment[0]['value']; ?><br />
		<span class="label">Occupation:</span><?php print render ($content['field_occupation']); ?><br />
		<span class="label">First came to AMIDEAST:</span><?php print render($content['field_first_to_amideast']); ?><br />
		
		</div>
		
		<div id="profile2space"></div>
	
	
	</div>
</div>

<div class="profile2Intro"><?php print render($content['field_intro_p']); ?></div> 
  <?php print render($content['body']); ?>
   
  
  
  
  
  

  
  
   <!-- content editbale ends here -->
   
   
   
   
   
   
   
   
</div> <!--/.content clear block -->


    
    </div> <!-- /.clear-block -->


</div> <!-- / #node-[nid] -->
