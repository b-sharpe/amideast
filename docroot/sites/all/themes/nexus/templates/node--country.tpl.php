<div class="translation_links"><?php print render($content['links']);?></div>		
<div class="countrybody">
<div class="countryleftheader">
<div class="countrytopbox"> <!-- Print PHP code for box -->

<?php
   print render($content['field_box_content']);

?>
<?php 
	global $language;
			if ($language->language == 'ar')
				{
				$more = "أكثر";
				}
					
			else 
				{
				 $more = "More";
				}
			
	?>

 </div>


<!-- Print PHP code for main text  in country header--> 

<?php

print render($content['field_summary']);

?>

</div>
<table cellspacing="0" cellpadding="10" border="0" align="center" width="100%" bgcolor="#FFFFFF">
    <tbody>
        <tr>
            <td width="33%" align="left" valign="top" bgcolor="#ffffff" class="bottom_right_2">
            <!--- insert code for TOP LEFT  section here --->   
<?php
	    print render($content['field_left_top']);
// print $node->field_left_top_links[0]['value'];

?>

<?php if (isset($content['field_left_top_links'])): ?>

	<?php 
		drupal_add_js('misc/form.js');
	    drupal_add_js('misc/collapse.js');
	?>

    <fieldset class="collapsible collapsed">
	  <legend><span class="fieldset-legend"><?php print $more ?></span></legend>
	<div class="fieldset-wrapper"> <?php print render($content['field_left_top_links']); ?> </div>  





</fieldset>

<?php endif; ?>

             </td>




           
            <td width="33%" align="left" valign="top" bgcolor="#ffffff" class="bottom_right_3">
            <!--- insert code for TOP MIDDLE section here --->    
	
<?php print render($content['field_middle_top']); ?>

<?php if (isset($content['field_middle_top_links'])): ?>

	<?php drupal_add_js('misc/collapse.js'); ?>
	<fieldset class="collapsible collapsed">
	  <legend><span class="fieldset-legend"><?php print $more ?></span></legend>
	<div class="fieldset-wrapper"> <?php print render($content['field_middle_top_links']); ?> </div>

</fieldset>

<?php endif; ?>









          </td>
            
              <td width="33%" align="left" valign="top" bgcolor="#ffffff" class="bottom_2">
              <!--- insert code for TOP RIGHT section here --->  
<?php print render($content['field_right_top']); ?>

<?php if (isset($content['field_right_top_links'])): ?>

	<?php drupal_add_js('misc/collapse.js'); ?>
	<fieldset class="collapsible collapsed">
	  <legend><span class="fieldset-legend"><?php print $more ?></span></legend>
	<div class="fieldset-wrapper"> <?php print render($content['field_right_top_links']); ?> </div>

</fieldset>

<?php endif; ?>







            </td>
        </tr>
        <tr>
            <td width="33%" align="left" valign="top" bgcolor="#ffffff" class="right_3">
            <!--- insert code for BOTTOM LEFT section here ---> 
<?php print render($content['field_left_bottom']); ?>

<?php if (isset($content['field_left_bottom_links'])): ?>

	<?php drupal_add_js('misc/collapse.js'); ?>
	<fieldset class="collapsible collapsed">
	  <legend><span class="fieldset-legend"><?php print $more ?></span></legend>
	<div class="fieldset-wrapper"> <?php print render($content['field_left_bottom_links']); ?> </div>

</fieldset>


<?php endif; ?>




            </td>
       
            <td width="33%" align="left" valign="top" bgcolor="#f6f6f6" class="right_2">
             <!--- insert code for BOTTOM MIDDLE section here ---> 

<?php print render($content['field_middle_bottom']); ?> 

<?php if (isset($content['field_middle_bottom_links'])): ?>

	<?php drupal_add_js('misc/collapse.js'); ?>
	<fieldset class="collapsible collapsed">
	  <legend><span class="fieldset-legend"><?php print $more ?></span></legend>
	<div class="fieldset-wrapper"> <?php print render($content['field_middle_bottom_links']); ?> </div>

</fieldset>

<?php endif; ?>




            </td>
            <td width="33%" align="left" valign="top" bgcolor="#ffffff"  class="right_4">
            <!--- insert code for BOTTOM RIGHT section here --->     
<?php print render($content['field_right_bottom']); ?>

<?php if (isset($content['field_right_bottom_links'])): ?>

	<?php drupal_add_js('misc/collapse.js'); ?>
	<fieldset class="collapsible collapsed">
	  <legend><span class="fieldset-legend"><?php print $more ?></span></legend>
	<div class="fieldset-wrapper"> <?php print render($content['field_right_bottom_links']); ?> </div>

</fieldset>

<?php endif; ?>





            </td>
        </tr>
    </tbody>
</table>
</div>
