<?php
// $Id: node.tpl.php,v 1.5 2007/10/11 09:51:29 goba Exp $
?>
<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?>">

<?php // print $picture ?>

<?php if ($page == 0): ?>
  <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
<?php endif; ?>

  <?php if ($submitted): ?>
    <span class="submitted"><?php // print $submitted; ?></span>
  <?php endif; ?>

 <div class="clear-block">
 <div class="meta">

	<div class="links"><?php print render($content['links']);?></div>
 



    </div>

<div class="content clear-block">


     <?php 
   if (!empty($content['field_banner_image'])):?>
	   
   
   <div id="picinset">
   <span class="tag6">
   <?php 
   print render ($content['field_banner_image']);
   
   ?>
   <?php if (!empty($content['field_caption_banner_image'])){
  print '<div class="caption"><em>';
  print render ($content['field_caption_banner_image']);
  print '</em></div>'; 
   
   }
   ?>
   
   </span></div>
 
   
   <?php endif; ?>
   
   <div class="program-logos">
   		<?php print render($content['field_logos']); 
		 ;?> 
	</div>
   <?php print render ($content['field_test_bool'])?>

   
     <div class="intro-paragraph">
	 	<?php print render ($content['field_intro_p']); ?>   
   
  	 </div>
  
    <?php print render($content['body']); ?>
   
</div>


    <?php 
	if (isset($taxonomy)): ?>
	<div class="terms"> Tagged under: <?php print $terms ?></div>
         <?php endif;?>
    </div>


</div>
  <?php print render($content['field_video']); ?> 
  
 