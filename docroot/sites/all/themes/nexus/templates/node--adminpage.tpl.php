<?php
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>

  <?php print $user_picture; ?>

  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <?php if ($display_submitted): ?>
    <span class="submitted"><?php print $submitted ?></span>
  <?php endif; ?>

  <div class="content clearfix"<?php print $content_attributes; ?>>
   <div class="links">  <?php 
	print render($content['links']);
	?></div>
     <?php 
   if (!empty($content['field_banner_image'])):?>
	   
   
   <div id="picinset">
   <span class="tag6">
   <?php 
   print render ($content['field_banner_image']);
   
   ?>
   <?php if (!empty($content['field_caption_banner_image'])){
  print '<div class="caption"><em>';
  print render ($content['field_caption_banner_image']);
  print '</em></div>'; 
   
   }
   ?>
   
   </span></div>
 
   
   <?php endif; ?>
    <?php 
	print render($content);
	?>
  </div>
   <?php // if (!empty($content['links'])):  ?>
      <div class="links"><?php // print render($content['links']); ?></div>
    <?php // endif; ?>
  <div class="clearfix">
  
  </div>

</div>

